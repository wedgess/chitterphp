<?php

require __DIR__ . '/vendor/autoload.php';

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

// report all errors
error_reporting(-1);
ini_set('display_errors', 'On');

require_once './DBHandler.php';
require_once './FCM.php';

// get a user object by UID
// response will be error, message if there was an error or error, user if succesful
$app->get('/user/info/{uid}', function ($request, $response, $args) {
    $db = new DBHandler();
    $uid = $args['uid'];
    sendResponse($response, $db->getUserById($uid));
});

$app->get('/user/messages/{uid}/{last_sync}', function ($request, $response, $args) {
    $db = new DBHandler();
    $uid = $args['uid'];
    $last_sync = $args['last_sync'];
    $isNow = false;
    if ($last_sync == "now") {
        $isNow = true;
        $last_sync = getServerTimestamp();
    }
    sendResponse($response, $db->getMessagesSinceLastSync($last_sync, $uid, $isNow));
});

$app->get('/user/chats/{uid}', function ($request, $response, $args) {
    $db = new DBHandler();
    $uid = $args['uid'];
    sendResponse($response, $db->getUsersChats($uid));
});

// Check to see if a user exists by phone number, response will be true or false
$app->get('/user/exist/{phone_number}', function ($request, $response, $args) {
    $db = new DBHandler();
    $phone_number = $args['phone_number'];
    sendResponse($response, $db->userExists($phone_number));
});

// Used to update a messages status, 0 - pending, 1 - delivered to server, 2 - delivered to device, 3 - message read
// response will be whether thaere was an error or not (error) and then message
$app->get('/message/status/{mid}/{status}', function ($request, $response, $args) {
    $db = new DBHandler();
    $mid = $args['mid'];
    $m_status = $args['status'];
    // get the message from DB
    $msg_response = $db->getMessageById($mid);
    // if no error in msg response
    if (!$msg_response['error']) {
        // get messages current status
        $cur_status = $msg_response['message']['m_status'];
        // make sure new status is not less than current, if it is send the current status to client
       // if ($m_status > $cur_status) {
            sendResponse($response, $db->updateMessageStatus($mid, $m_status));
//        } else {
//            $res['error'] = true;
//            $res['message'] = "Decreasing message status not allowed!!";
//            sendResponse($response, $res);
//        }
    }
});

// all users contacts will be sent to here, each one will be checked to see if registered
// if not registered the contact is skipped otherwise we get user info and send array of
// contacts back to client in response
$app->post('/user/contacts', function ($request, $response) {
//    $json = $request->getBody();
//    $data = json_decode($json, true);
    $data = $request->getParsedBody();
    //print_r($data);
    $result = array();
    $db = new DBHandler();
    //print_r($data);
    // for each item jey is name and phone_num is contacts number
    foreach ($data as $key => $phone_num) {
        //print_r($user);
        // make sure the contact is registered with Chitter App, if not ignore them
        if ($db->userExists($phone_num)) {
            // replace phone number etc to get contact name as is on users device
            $contact_name = str_replace("u_phone_number_", "", $key);
            // get the user by their phone number
            $user = $db->getUserByPhoneNumber($phone_num);
            // store contacts name in user object
            $user['u_name'] = $contact_name;
            // add object to the array as the array will be sent back to users device
            array_push($result, $user);
        }
    }
    //print_r($result);
    sendResponse($response, $result);
});

// update users last seen status
$app->put('/user/last_seen', function ($request, $response) {
    $data = $request->getParsedBody();
    $db = new DBHandler();
    $uid = $data['u_id'];
    $last_seen = $data['u_last_seen'];
    // if last seen is not online use servers timestamp
    if ($last_seen != 'Online') {
        $last_seen = getServerTimestamp();
    }
    $result = $db->updateUserLastSeen($uid, $last_seen);
    $db->notifyContactsOfStatusChanged($uid, $last_seen);
    sendResponse($response, $result);
});

// update users last seen status
$app->put('/user/public/info', function ($request, $response) {
    $data = $request->getParsedBody();
    $db = new DBHandler();
    $uid = $data['u_id'];
    $username = $data['u_name'];
    $userStatus = $data['u_status'];
    $userImgUrl = $data['u_profile_img_url'];
    // remove profile info from array so we only have contact ids remaining
    unset($data['u_id']);
    unset($data['u_name']);
    unset($data['u_status']);
    unset($data['u_profile_img_url']);
    
    $result = $db->updateUserPublicInfo($uid, $username, $userStatus, $userImgUrl);
    
    foreach ($data as $key => $contact_id) {
        $db->notifyContactsOfProfileChanged($contact_id, $uid, $username, $userStatus, $userImgUrl);        
    }    
    
    sendResponse($response, $result);
});


// used to update a users FCM TOKEN
$app->put("/user/fcm_token", function ($request, $response) {
    $data = $request->getParsedBody();

    // check that the required params are in the request
    if (hasRequiredParams($response, $data, array('u_id', 'u_fcm_token'))) {
        $db = new DBHandler();
        $fcm_token = filter_var($data['u_fcm_token'], FILTER_SANITIZE_STRING);
        $uid = filter_var($data['u_id'], FILTER_SANITIZE_NUMBER_INT);
        sendResponse($response, $db->updateFcmID($uid, $fcm_token));
    }
});

// uploads media to server, NOTE - Directory /media/uploads must be present on on server inside project
$app->post('/media/upload', function ($request, $response) {
    // if FILES array is empty or error is set we have an error
    if (empty($_FILES) || $_FILES['file']['error']) {
        $result["error"] = true;
        $result["message"] = "failed to move uploaded file";
        sendResponse($response, $result, 400);
        return;
    }
    //print_r($_FILES);
    $error = false;
    // parse the request body so we can access its keys
    $data = $request->getParsedBody();

    // if chunks aren;t set deafult to 0
    $chunk = isset($data["chunk"]) ? intval($data["chunk"]) : 0;
    $chunks = isset($data["chunks"]) ? intval($data["chunks"]) : 0;

    // if name is set in request then use that as file name otherwise get it from file global variable
    $fileName = isset($data["name"]) ? $data["name"] : $_FILES["file"]["name"];
    
    if ($fileName == NULL) {
        $fileName = "Deadly.jpg";
    }

    // the file path of project to upload to followed by the name of the file
    $filePath = "media/uploads/$fileName";

    // Open temp file - write binary/append binary - depending on chunk value
    $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");

    if ($out) {
        // Read binary input stream and append it to temp file
        $in = @fopen($_FILES['file']['tmp_name'], "rb");

        if ($in) {
            // read the input stream and output it to temp file
            while ($buff = fread($in, 4096)) {
                fwrite($out, $buff);
            }
        } else {
            // error with input stream
            $result["error"] = true;
            $result["message"] = "Oops!  Failed to open input Stream error occurred.";
            sendResponse($response, $result, 400);
            $error = true;
        }
    } else {
        // error opening file
        $result["error"] = true;
        $result["message"] = "Oops! Failed to open output error occurred.";
        sendResponse($response, $result, 400);
        $error = true;
    }

    // close file so we can rename it later or remove if error
    @fclose($in);
    @fclose($out);
    @unlink($_FILES['file']['tmp_name']);

    if ($error) {
        // we have an error so delete the file
        //@unlink("{$filePath}.part");
        return;
    }

    // Check if file has been uploaded
    if (!$chunks || $chunk == $chunks - 1) {
        // Strip the temp .part suffix off the file
        rename("{$filePath}.part", $filePath);
    }
    // send response back to server with the uploaded file path
    $result["error"] = false;
    $result["message"] = "successfully uploaded: {$filePath}";
    sendResponse($response, $result);
});

// used for logging users in
$app->post('/user/login', function ($request, $response) {
    // get the body data from request
    $data = $request->getParsedBody();
    //print_r($data);
    // check that the required params are in the request
    if (hasRequiredParams($response, $data, array('u_phone_number', 'u_password'))) {

        $password = filter_var($data['u_password'], FILTER_SANITIZE_STRING);
        $phone_num = filter_var($data['u_phone_number'], FILTER_SANITIZE_STRING);

        $db = new DBHandler();
        $dbResult = array();
        if ($db->userExists($phone_num)) {
            // get user array from DB
            $dbResult['user'] = $db->getUserByPhoneNumber($phone_num);


            if ($dbResult['user']['u_password'] == $password) {
                //echo "Correct password";
                $dbResult['error'] = false;
            } else {
                //echo "Passwords do not match";
                // passwords don't match so wipe the array and return error response
                $dbResult = array();
                $dbResult['error'] = true;
                $dbResult['message'] = 'Incorrect password!';
            }
        } else {
            $dbResult = array();
            $dbResult['error'] = true;
            $dbResult['message'] = 'Phone Number not registered!';
        }
        sendResponse($response, $dbResult, 200);
    }
});

// used for registering new users
$app->post('/user/register', function ($request, $response) {
    // get the body data from request
    $json = $request->getBody();
    $data = json_decode($json, true);
    // for debugging
    //print_r($data);
    //$data = $request->getParsedBody();
    //print_r($data);
    // check that the required params are in the request, img_url is not required
    if (hasRequiredParams($response, $data, array('u_name', 'u_phone_number', 'u_password', 'u_fcm_token', 'u_status'))) {

        $name = filter_var($data['u_name'], FILTER_SANITIZE_STRING);
        $phone_num = filter_var($data['u_phone_number'], FILTER_SANITIZE_STRING);
        $img_url = "";
        // check if img_url is set, if not set as empty
        if (isset($data['u_profile_img_url'])) {
            $img_url = $data['u_profile_img_url'];
        }
        $password = filter_var($data['u_password'], FILTER_SANITIZE_STRING);
        // don;t filter for now just to test that it doesn't mess up the token
        $fcm_token = $data['u_fcm_token'];
        $status = filter_var($data['u_status'], FILTER_SANITIZE_STRING);

        $db = new DBHandler();
        if (!$db->userExists($phone_num)) {
            // get user array from DB
            $dbResult = $db->createUser($name, $phone_num, $password, $fcm_token, $img_url, $status);
            sendResponse($response, $dbResult, 200);
        }
//        else {
//            sendResponse($response, $dbResult, 200);
//        }
    }
});


// chats


$app->get('/chats/delete/{cid}', function ($request, $response, $args) {
    echo "deleting chats...";
    $db = new DBHandler();
    $cid = $args['c_id'];
    sendResponse($response, $db->deleteChat($cid));
});

$app->get('/chats/{cid}', function ($request, $response, $args) {
    $db = new DBHandler();
    $cid = $args['cid'];
    sendResponse($response, $db->getChatInfo($cid));
});


// TODO: Need to check if this room doesn;t already exist
// used for creating new chats
$app->post('/chats/create', function ($request, $response) {
    // get the body data from request
    $json = $request->getBody();
    $data = json_decode($json, true);
    // for debugging
    //print_r($data);
    //$data = $request->getParsedBody();
    //print_r($data);
    // check that the required params are in the request, img_url is not required
    if (hasRequiredParams($response, $data, array('c_id', 'c_created_by')) && isset($data['c_members'])) {

        if (isset($data['c_name'])) {
            $name = filter_var($data['c_name'], FILTER_SANITIZE_STRING);
        } else {
            $name = "";
        }

        $cid = filter_var($data['c_id'], FILTER_SANITIZE_STRING);

        $created_by = filter_var($data['c_created_by'], FILTER_SANITIZE_NUMBER_INT);
        // check if img_url is set, if not set as empty
        if (isset($data['c_img_url'])) {
            $url = filter_var($data['c_img_url'], FILTER_SANITIZE_STRING);
            $img_url = $url;
        } else {
            $img_url = "";
        }

        $db = new DBHandler();
        $dbResult = $db->createChatRoom($cid, $name, $img_url, $created_by);
        if (!$dbResult['error']) {
            // set the chat members IDs as the members we sent in the request
            $cid = $cid;
            foreach ($data['c_members'] as $uid) {
                $db->createUserChats($cid, $uid);
            }
            $dbResult['chat']['c_members'] = $data['c_members'];
        }

        // get chat array from DB
        sendResponse($response, $dbResult, 200);
    } else {
        echo "Required params created_by or members is missing..";
    }
});

// check if a chat between the 2 users exists
$app->post("/message/create", function ($request, $response) {
    $data = $request->getParsedBody();

    // check that the required params are in the request
    // TODO: decide if message is required or not because if an image message will be empty
    if (hasRequiredParams($response, $data, array('m_id', 'm_message', 'm_cid', 'm_sender_id', 'm_status'))) {
        $db = new DBHandler();
        $mid = filter_var($data['m_id'], FILTER_SANITIZE_STRING);
        $message = filter_var($data['m_message'], FILTER_SANITIZE_STRING);
        // the " and ' are encoded when sent in the request so replace them
        $message = str_replace("&#39;", "'", $message);
        $message = str_replace("&#34;", '"', $message);
        //$status = filter_var($data['m_status'], FILTER_SANITIZE_NUMBER_INT);
        $sender_id = filter_var($data['m_sender_id'], FILTER_SANITIZE_NUMBER_INT);
        $img_url = "";
        $cid = filter_var($data['m_cid'], FILTER_SANITIZE_STRING);
        $result = $db->addMessage($mid, $sender_id, $cid, $message, $img_url);
        $fcm = new FCM();
        // TODO: get FCM tokens of users in the chat room
        //$to = $db->getUserById($recipient_id)['fcm_token'];
        $to = $db->getChatMembersFcm($cid, $sender_id);
        print_r($to);
        if (!$result['error']) {
            $to_length = count($to);
            echo "To Length: {$to_length}";
            if ($to_length == 1) {
                $result['message']['title'] = $sender_id;
                //print_r($result['message']);
                echo $fcm->send($to[0], $result['message']);
            } else if ($to_length > 1) {
                $chatResponse = $db->getChatById($cid);
                $chat = $chatResponse['chat'];
                $result['message']['title'] = $chat['c_name'];
                echo $fcm->sendMultiple($to, $result['message'], $chat['c_name']);
            }
        }
        sendResponse($response, $result);
    }
});

///// Helper Methods
/**
 * Verifying required params sent in the request
 * @param type $required_fields -- array of the required fields
 */
function hasRequiredParams($response, $requestBody, $required_fields) {
    $valid = true;
    $error_fields = "";
    foreach ($required_fields as $field) {
        if (!isset($requestBody[$field]) || strlen(trim($requestBody[$field])) <= 0) {
            $valid = false;
            $error_fields .= $field . ', ';
        }
    }

    if (!$valid) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response_array = array();
        $response_array["error"] = true;
        $response_array["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        //print_r($response_array);
        sendResponse($response, $response_array, 400);
    }

    return $valid;
}

function sendResponse($response, $response_array, $status = 200) {
    return $response->withJSON($response_array, $status);
}

function getServerTimestamp() {
    return gmdate('Y-m-d H:i:s');
}

$app->run();
?>
