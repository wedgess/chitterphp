<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 * Get chat id from 2 user ids
 * SELECT cid FROM user_chats WHERE uid = 1 OR uid = 3 GROUP BY cid HAVING COUNT(uid) = 2
 */

class DBHandler {

    private $connection;

    function __construct() {
        require_once './DBConnection.php';
        // create instance of our DBConnection class
        $db = new DBConnection();
        // store the db connection in this classes reference to the db connection
        $this->connection = $db->connect();
    }

    // creating new user
    public function createUser($name, $phone_num, $password, $fcm_token, $img_url, $u_status) {
        // create a new array which holds the response data
        $response = array();

        // insert user query
        $statement = $this->connection->prepare("INSERT INTO users(u_name, u_phone_number, u_password, u_fcm_token, u_profile_img_url, u_status) values(:name, :phone_num, :password, :fcm_token, :img_url, :status)");

        // fill the parameters for the placeholders in the query with values passed in paramters
        $params = array(
            "name" => $name,
            "phone_num" => $phone_num,
            "password" => $password,
            "fcm_token" => $fcm_token,
            "img_url" => $img_url,
            "status" => $u_status
        );
        $status = $statement->execute($params);
        //print_r($statement->errorInfo());
        if ($status) {
            // User successfully inserted
            $response["error"] = false;
            $response["user"] = $this->getUserByPhoneNumber($phone_num);
        } else {
            // Failed to create user
            $response["error"] = true;
            $response["message"] = print_r($statement->errorInfo()) . "Oops! An error occurred while registering new user";
            //$response["message"] = "Oops! An error occurred while registering";
        }

        return $response;
    }

    /**
     * Checking if user exists by phone number
     * @param String $phone_num - number to check in db
     * @return boolean
     */
    public function userExists($phone_num) {
        // SQL query for getting a user by email
        $statement = $this->connection->prepare("SELECT u_id from users WHERE u_phone_number = :phone_num");

        // fill the parameters for the placeholder in the query with values from $params array
        $params = array(
            "phone_num" => $phone_num
        );

        // execute the query
        $status = $statement->execute($params);

        if (!$status) {
            die("Could not read user with phone number: {$phone_num}");
        }

        // if the user exists rowCount() will be greater than 0
        return $statement->rowCount() > 0;
    }

    /**
     * Fetching user by email
     * @param String $phone_num User phone number
     */
    public function getUserByPhoneNumber($phone_num) {
        $statement = $this->connection->prepare("SELECT * FROM users WHERE u_phone_number = :phone_num");
        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array("phone_num" => $phone_num);

        $status = $statement->execute($params);
        //print_r($statement->errorInfo());
        // status is false then the attendee was not inserted
        if ($status) {
            //die("Could not read user");
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            //print_r($row);
            return $row;
        } else {
            $response["error"] = true;
            $response["message"] = "User with phone number: " . $phone_num . " not found!";
            return $response;
        }
    }

    public function getUserById($uid) {
        $statement = $this->connection->prepare("SELECT * FROM users WHERE u_id = :uid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "uid" => $uid
        );

        $status = $statement->execute($params);
        //print_r($statement->errorInfo());
        // status is false then the attendee was not inserted
        if ($status) {
            //die("Could not read user");
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            $response["error"] = false;
            $response["user"] = $row;
            return $response;
        } else {
            $response["error"] = true;
            $response["message"] = "User with id:{$uid} not found!";
            return $response;
        }
    }

    // updating user FCM registration ID
    public function getFCMTokenByUid($uid) {
        $response = array();

        $statement = $this->connection->prepare("SELECT u_fcm_token FROM users WHERE u_id = :uid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "uid" => $uid
        );
        $status = $statement->execute($params);
        if ($status) {
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            $response["error"] = false;
            $response["token"] = $row['u_fcm_token'];
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to get users FCM registration ID";
        }

        return $response;
    }

    // updating user FCM registration ID
    public function updateFcmID($uid, $fcm_token) {
        $response = array();

        $statement = $this->connection->prepare("UPDATE users SET u_fcm_token = :token WHERE u_id = :uid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "token" => $fcm_token,
            "uid" => $uid
        );
        $status = $statement->execute($params);
        if ($status) {
            $response["error"] = false;
            $response["message"] = 'FCM registration ID updated successfully';
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to update FCM registration ID";
        }

        return $response;
    }

    // updating message status, for read receipts etc
    public function updateMessageStatus($mid, $msg_status) {
        $response = array();

        $statement = $this->connection->prepare("UPDATE messages SET m_status = :m_status WHERE m_id = :mid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "m_status" => $msg_status,
            "mid" => $mid
        );
        $status = $statement->execute($params);
        if ($status) {
            // get the message object
            $messageResult = $this->getMessageById($mid);
            // if message result doesn;t have an error
            if (!$messageResult['error']) {
                // get the senders info
                $senderResult = $this->getUserById($messageResult['message']['m_sender_id']);
                // if the sender info has no error
                if (!$senderResult['error']) {
                    // get senders FCM token
                    $fcm_token = $senderResult['user']['u_fcm_token'];
                    // send a receipt back to the sender
                    $fcm = new FCM();
                    $fcm->sendReceipt($fcm_token, $messageResult['message'], $msg_status);
                    $response["error"] = false;
                    $response["message"] = "Updated message ({$mid}) to status: {$msg_status}";
                }
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to update message ({$mid}) to status {$msg_status}";
            }
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to update users last seen";
        }

        return $response;
    }

    // updating users last seen field
    public function updateUserLastSeen($uid, $last_seen) {
        $response = array();

        $statement = $this->connection->prepare("UPDATE users SET u_last_seen = :last_seen WHERE u_id = :uid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "last_seen" => $last_seen,
            "uid" => $uid
        );
        $status = $statement->execute($params);
        if ($status) {
            $response["error"] = false;
            $response["user"] = $this->getUserById($uid);
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to update users last seen";
        }

        return $response;
    }

    // updating users last seen field
    public function updateUserPublicInfo($uid, $username, $userstatus, $user_img) {
        $response = array();

        $statement = $this->connection->prepare("UPDATE users SET u_name = :uname, u_status = :ustatus, u_profile_img_url = :uprofile_img_url WHERE u_id = :uid");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "uname" => $username,
            "ustatus" => $userstatus,
            "uprofile_img_url" => $user_img,
            "uid" => $uid
        );
        $status = $statement->execute($params);
        if ($status) {
            $response["error"] = false;
            $response["user"] = $this->getUserById($uid)['user'];
        } else {
            $response["error"] = true;
            $response["message"] = "Failed to update users public profile data";
        }

        return $response;
    }

    //messages
    // messaging in a chat room / to person message
    public function addMessage($mid, $uid, $cid, $message, $img_url) {
        $response = array();

        $statement = $this->connection->prepare("INSERT INTO messages (m_id, m_cid, m_sender_id, m_message, m_media_url, m_status) values(:mid, :cid, :uid, :msg, :img_url, :stat)");

        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "mid" => $mid,
            "cid" => $cid,
            "uid" => $uid,
            "msg" => $message,
            "img_url" => $img_url,
            "stat" => '1'
        );

        $status = $statement->execute($params);

        try {
            if ($status) {
                $response = $this->getMessageById($mid);
            } else {
                $response['error'] = true;
                $response['message'] = print_r($statement->errorInfo()) . " ::Failed to send message: {$mid}";
            }
        } catch (PDOException $e) {
            $response['error'] = true;
            $response['message'] = "Failed to send message: {$mid}  ERROR: {$e->getCode()} )";
        }

        return $response;
    }

    /**
     * Fetching user by email
     * @param String $mid User phone number
     */
    public function getMessageById($mid) {
        $statement = $this->connection->prepare("SELECT * FROM messages WHERE m_id = :mid");
        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array("mid" => $mid);
        $status = $statement->execute($params);
        //print_r($statement->errorInfo());
        // status is false then the attendee was not inserted
        if ($status) {
            //die("Could not read user");
            $row = $statement->fetch(PDO::FETCH_ASSOC);

            $response['error'] = false;
            $response['message'] = $row;
        } else {
            $response["error"] = true;
            $response["message"] = "Message with id:{$mid} not found!";
        }
        return $response;
    }

    // create chat room
    // creating new user
    public function createChatRoom($cid, $name, $img_url, $created_by) {
        // create a new array which holds the response data
        $response = array();

        // insert user query
        $statement = $this->connection->prepare("INSERT INTO chats(c_id, c_name, c_img_url, c_created_by) values(:cid, :name, :img_url, :created_by)");

        // fill the parameters for the placeholders in the query with values passed in paramters
        $params = array(
            "cid" => $cid,
            "name" => $name,
            "img_url" => $img_url,
            "created_by" => $created_by
        );
        $status = $statement->execute($params);

        //print_r($statement->errorInfo());
        if ($status) {
            // User successfully inserted
            $response = $this->getChatById($cid);
        } else {
            // Failed to create user
            $response["error"] = true;
            $response["message"] = print_r($statement->errorInfo()) . "Oops! An error occurred while creating new chat room";
        }

        return $response;
    }

    /**
     * Fetching user by email
     * @param String $cid User phone number
     */
    public function getChatById($cid) {
        $statement = $this->connection->prepare("SELECT * FROM chats WHERE c_id = :cid");


        // fill the parameters for the placeholders in the query with values from attendee object
        $params = array(
            "cid" => $cid
        );

        $status = $statement->execute($params);
        //print_r($statement->errorInfo());
        // status is false then the attendee was not inserted
        if ($status) {
            //die("Could not read user");
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            //print_r($user);
            $response["error"] = false;
            $response["chat"] = $row;
        } else {
            $response["error"] = true;
            $response["message"] = "Chat with id:{$cid} not found!";
        }
        return $response;
    }

    public function deleteChat($cid) {
        $result = array();
        $statement = $this->connection->prepare("DELETE FROM users_chats WHERE cid = :cid");
        // fill the parameters for the placeholder in the query with values from $params array
        $params = array("cid" => $cid);

        // execute the query
        $status = $statement->execute($params);
        if ($status) {
            $result['error'] = false;
            $result['message'] = "Chat with id:{$cid} deleted";
        } else {
            $result['error'] = true;
            $result['message'] = "Something went wrong while deleteing chat with id:{$cid}";
        }

        $statement2 = $this->connection->prepare("DELETE FROM chats WHERE c_id = :cid");
        $status2 = $statement2->execute($params);

        if ($status) {
            if ($status2) {
                $result['error'] = false;
                $result['message'] = "Chat with id:{$cid} and associated tables deleted";
            } else {
                $result['error'] = true;
                $result['message'] = "Something went wrong while deleteing associated chat with id:{$cid} with users_chats";
            }
        }

        // if the user exists rowCount() will be greater than 0
        return $result;
    }

    public function createUserChats($cid, $uid) {
        // create a new array which holds the response data
        $response = array();

        // insert user query
        $statement = $this->connection->prepare("INSERT INTO users_chats(cid, uid) values(:cid, :uid)");

        // fill the parameters for the placeholders in the query with values passed in paramters
        $params = array(
            "cid" => $cid,
            "uid" => $uid
        );
        $status = $statement->execute($params);

        //print_r($statement->errorInfo());
//        if ($status) {
//            // User successfully inserted
//            $response["error"] = false;
//            $response["chat"] = $this->getChatById($cid);
//        } else {
//            // Failed to create user
//            $response["error"] = true;
//            $response["message"] = print_r($statement->errorInfo()) . "Oops! An error occurred while registering new user";
//        }
//
        return $status;
    }

    public function getChatMembersFcm($cid, $sender_id) {
        $statement = $this->connection->prepare("SELECT uid FROM users_chats WHERE uid <> :sid AND cid = :cid");
        $params = array("cid" => $cid, "sid" => $sender_id);
        $status = $statement->execute($params);
        if ($status) {
            $users = array();
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $result = $this->getFCMTokenByUid($row['uid']);
                if (!$result['error']) {
                    array_push($users, $result['token']);
                }
            }
//            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
//                array_push($users, $this->getUserById($row['uid']));
//            }
            return $users;
        }
    }

    public function getMessagesSinceLastSync($last_sync, $uid, $isNow) {
        if ($isNow) {
            $query = "SELECT * FROM messages where m_created_at < :last_sync AND m_sender_id <> :uid AND messages.m_status <> 0 AND messages.m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = :uid) AND users_chats.uid <> :uid GROUP BY users_chats.cid) ORDER BY m_created_at ASC";
        } else {
            $query = "SELECT * FROM messages where m_created_at > :last_sync AND m_sender_id <> :uid AND messages.m_status <> 0 AND messages.m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = :uid) AND users_chats.uid <> :uid GROUP BY users_chats.cid) ORDER BY m_created_at ASC";
        }
        // SELECT * FROM messages where m_created_at > "2017-02-17 22:25:58" AND m_sender_id <> 1 AND messages.m_status <> 0 AND messages.m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = 1) AND users_chats.uid <> 1 GROUP BY users_chats.cid) ORDER BY m_created_at ASC
        $statement = $this->connection->prepare($query);
        $params = array(
            "uid" => $uid,
            "last_sync" => $last_sync
        );
        $status = $statement->execute($params);
        $result = array();

        if ($status) {
            $messages = array();
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($messages, $row);
            }
            $result['error'] = false;
            $result['messages'] = $messages;
        } else {
            $result['error'] = true;
            $result['message'] = "Error getting messages since {$last_sync} for user with id: {$uid}";
        }

        return $result;
    }

    public function getUsersChats($uid) {
        $statement = $this->connection->prepare("SELECT * FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = :uid) AND uid <> :uid");
        $params = array("uid" => $uid);
        $status = $statement->execute($params);
        $result = array();
        $users_chats = array();
        if ($status) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($users_chats, $row);
            }
            $result['error'] = false;
            $result['users_chats'] = $users_chats;
        } else {
            $result['error'] = true;
            $result['message'] = "Error getting chats for user with id: {$uid}";
        }

        return $result;
    }

    // 1 - status change - last online
    // 2 - status changed - fcm token updated
    // SELECT uc.uid FROM users_chats uc WHERE uc.cid IN (SELECT cid FROM `users_chats` WHERE uid IN (SELECT uid FROM `users_chats` WHERE uid = 1)) AND uc.uid <> 1 GROUP BY uid
    public function notifyContactsOfStatusChanged($uid, $last_seen) {
        $statement = $this->connection->prepare("SELECT uc.uid FROM users_chats uc WHERE uc.cid IN (SELECT cid FROM users_chats WHERE uid IN (SELECT uid FROM users_chats WHERE uid = :uid)) AND uc.uid <> :uid GROUP BY uid");
        $params = array("uid" => $uid);
        $status = $statement->execute($params);
        $res = array();
        if ($status) {
            $fcm = new FCM();
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $result = $this->getFCMTokenByUid($row['uid']);
                if (!$result['error']) {
                    $res['error'] = false;
                    $res['message'] = "Successfully sent status update to contacts";
                    $fcm->sendLastOnlineUpdate($result['token'], $uid, $last_seen);
                } else {
                    $res['error'] = true;
                    $res['message'] = "Failed sending status update to contacts, error getting fcm token for {$row['uid']}";
                }
            }
        } else {
            $res['error'] = true;
            $res['message'] = "original statement is false";
        }
        return $res;
    }

    public function notifyContactsOfProfileChanged($contact_id, $uid, $username, $userstatus, $userImgUrl) {
        $fcm = new FCM();
        $result = $this->getFCMTokenByUid($contact_id);
        if (!$result['error']) {
            $res['error'] = false;
            $res['message'] = "Successfully sent status update to contacts";
            $fcm->sendProfileInfoUpdate($result['token'], $uid, $username, $userstatus, $userImgUrl);
        } else {
            $res['error'] = true;
            $res['message'] = "Failed sending status update to contacts, error getting fcm token for {$contact_id}";
        }
    }

    public function getChatInfo($cid) {
        $result = array();
        $statement = $this->connection->prepare("SELECT * FROM `chats` LEFT JOIN users_chats on chats.c_id = users_chats.cid WHERE c_id = :cid");
        $params = array("cid" => $cid);
        $status = $statement->execute($params);
        if ($status) {
            $user_chats = array();
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $chat = array();
                $chat['c_id'] = $row['c_id'];
                $chat["c_name"] = $row['c_name'];
                $chat["c_img_url"] = $row['c_img_url'];
                $chat["c_created_by"] = $row['c_created_by'];
                $chat["c_created_at"] = $row['c_created_at'];
                $chat["uid"] = $row['uid'];
                array_push($user_chats, $chat);
            }
            $result['error'] = false;
            $result['message'] = $user_chats;
        } else {
            $result['error'] = true;
            $result['message'] = "Error retrieving info for chat with id: {$cid}";
        }
        return $result;
    }

}
