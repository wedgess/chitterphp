<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBCOnnection {

    private $conn; //instance of our connection

    function __construct() {
        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {
        include_once dirname(__FILE__) . '/Constants.php';

        $dsn = "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME;

        // Connecting to mysql database
        $this->conn = new PDO($dsn, DB_USERNAME, DB_PASSWORD);

        // Check for database connection error
        if (!$this->conn) {
            echo "Could not connect to DB";
        }

        // returing connection resource
        return $this->conn;
    }

}

?>
