<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FCM {
 
    // empty constructor
    function __construct() {  
    }
 
    /**
     * Used for sending push message to single user by fcm registration id
     * 
     * @param type $to - receivers FCM ID
     * @param type $message - string message for user
     * @return type
     */
    public function send($to, $message) {
        // FCM's data is a key value pair, so if we set $message as the data the JSOn will
        // be converted toa String which is not what we want cause we parse the json response
        // on the android client side
        $fields = array(
            'to' => $to,
            'data' => array('message_response' =>  $message, 'title' => $message['title']),
            'priority' => 'high',
            "time_to_live" => 0
        );
        return $this->sendPushNotification($fields);
    }
    
    public function sendReceipt($to, $mid, $msg_status) {
        
        $fields = array(
            'to' => $to,
            'data' => array("message_status" => $msg_status, "message_response" => $mid),
            'priority' => 'high',
            
        );        
        return $this->sendPushNotification($fields);
    }
    
    public function sendLastOnlineUpdate($token, $uid, $status) {
        $fields = array(
            'to' => $token,
            'data' => array("update_user_last_seen" => $status, "uid" => $uid),
            "collapse_key" => "update{$uid}"
        );        
        return $this->sendPushNotification($fields);
    }
    
    public function sendProfileInfoUpdate($token, $uid, $username, $userstatus, $userImgUrl) {
        $fields = array(
            'to' => $token,
            'data' => array("update_user_profile" => $uid, "u_name" => $username, "u_status" => $userstatus, "u_profile_img_url" => $userImgUrl),
            "collapse_key" => "updateProfile{$uid}"
        );        
        return $this->sendPushNotification($fields);
    }
 
    /**
     * Sending message to a topic by topic id
     * 
     * @param type $to - receivers FCM ID
     * @param type $message - string message for user
     * @return type
     *
     */
    public function sendToTopic($to, $message) {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => array('message_response' =>  $message),
        );
        return $this->sendPushNotification($fields);
    }
 
    // sending push message to multiple users by gcm registration ids
    /**
     * 
     * @param type $registration_ids - array of registration ids of users to send to
     * @param type $message - body of message users will receive
     * @return type
     */
    public function sendMultiple($registration_ids, $message, $title) {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => array('message_response' =>  $message, 'title' => $title),
            'priority' => 'high',
        );
 
        return $this->sendPushNotification($fields);
    }
 
    // function makes curl request to gcm servers
    private function sendPushNotification($fields) {
 
        // include config
        include_once __DIR__ . '/Constants.php';
 
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';
 
        $headers = array(
            'Authorization: key=' . FCM_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();
 
        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
//        print_r($fields);
//        echo json_encode($fields);
 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
 
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
 
        // Close connection
        curl_close($ch);
 
        return $result;
    }
 
}
 
?>

