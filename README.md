# Project Structure

### File Structure

* index.php - the REST API
* FCM.php - Used to push to FCM server
* DBConnection - Used to get DB connection
* DBHandler - Is the table gateway class to CRUD on the DB
* Constants.php - Application constants including FCM server token etc...


### Endpoints

| Endpoint                  | Method   | Params                  | Action                                      |
| :------------------------ |:--------:|:-----------------------:| :-------------------------------------------|
| user/login                | POST     | phonenumber & password  | Returns the User JSON if user exists.       |
| user/register             | POST     | User JSON object        | Returns the User JSON of registered user.   |
| user/{phone_number}       | GET      | phone_number            | Returns the User JSON                       |
| user/exist/{phone_number} | GET      | phone_number            | Returns true/ false                         |
| user/last_seen            | PUT      | uid & last_seen         | Updates user last_seen date                 |
| user/fcm_token            | PUT      | uid & fcm_token         | Updates users FCM token                     |
| user/contacts             | POST     | array of phone numbers  | Returns User JSON of contacts               |
| media/upload              | POST     | FILE                    | Returns PATH of uploaded media              |

