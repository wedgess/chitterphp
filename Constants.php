<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// FCM APi Key to connect to FCM
define("FCM_API_KEY", "AAAAFWaxhTY:APA91bEqPzILmiKJko5Tav01FZp-vcgJVmI1yCOl1N88LGfds-8muuBlhMU6BTrJoJ68ZPfrQhHveJTfezheAfxRiMQ3Ak7e9ri_5l3uA0cKKfj67gjl7qPHDpjFGZXlileC2xLm-6hTW1Hx1ZvX5QI5jyQUHfWsXg");

/**
 * Database configuration
 */
define('DB_USERNAME', 'chitteruser');
define('DB_PASSWORD', '');
define('DB_HOST', 'localhost');
define('DB_NAME', 'chitter');


// table chats
define('TABLE_CHATS', 'chats');
define('COLUMN_CHAT_ID', 'c_id');
define('COLUMN_CHAT_NAME', 'c_name');
define('COLUMN_CHAT_IMG_URL', 'c_img_url');
define('COLUMN_CHAT_CREATED_BY', 'c_created_by');
define('COLUMN_CHAT_CREATED_AT', 'c_created_at');


define('TABLE_USERS', 'users');
define('COLUMN_USER_ID', 'u_id');
define('COLUMN_USER_NAME', 'u_name');
define('COLUMN_USER_PHONE_NUMBER', 'u_phone_number');
define('COLUMN_USER_PASSWORD', 'u_password');
define('COLUMN_USER_FCM_TOKEN', 'u_fcm_token');
define('COLUMN_USER_CREATED_AT', 'u_created_at');
define('COLUMN_USER_LAST_SEEN', 'u_last_seen');

define('TABLE_MESSAGES', 'messages');
define('COLUMN_MESSAGE_ID', 'm_id');
define('COLUMN_MESSAGE_CHAT_ID', 'm_cid');
define('COLUMN_MESSAGE_MSG', 'm_message');
define('COLUMN_MESSAGE_MEDIA_URL', 'm_media_url');
define('COLUMN_MESSAGE_SENDER_ID', 'm_sender_id');
define('COLUMN_MESSAGE_CREATED_AT', 'm_created_at');
define('COLUMN_MESSAGE_STATUS', 'm_status');


define('TABLE_USERS_CHATS', 'users_chats');
define('COLUMN_USER_CHATS_UID', 'uid');
define('COLUMN_USER_CHATS_CID', 'cid');

